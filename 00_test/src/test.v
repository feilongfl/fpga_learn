module test(
	input i,
	output o
);

	assign o = i;

endmodule